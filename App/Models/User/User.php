<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 15/11/2018
 * Time: 13:39
 */

class User extends Model
{
    public function __construct()
    {
    }

    // Fonction d'enregistrement de l'utilisateur
    public static function register($array){
        $userpassword = $array['password'];
        //var_dump($userpassword);
        $hashpassword = password_hash($userpassword, PASSWORD_DEFAULT);
        $array['password'] = $hashpassword;

        try{
            $db = new PDO('mysql:host=localhost;dbname=garderie','damien','root');
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $userprepare = $db->prepare('INSERT INTO user (Name, Firstname, Address, Email, Password, Phone, Role_idRole) VALUES (:name, :firstname, :address, :email, :password, :phone, 1)' );
            foreach($array as $key => &$value) {
                $userprepare->bindParam(":" . $key, $value);
            }
            $userprepare->execute();
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }


    public static function registerPro($array){
        $propassword = $array['password'];
        //var_dump($propassword);
        $hashpropassword = password_hash($propassword, PASSWORD_DEFAULT);
        $array['propassword'] = $hashpropassword;

        try{
            $db = new PDO('mysql:host=localhost;dbname=garderie', 'damien', 'root');
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $proprepare = $db->prepare('INSERT INTO professional ( Company, Address, Email, Phone, Name, Firstname, Agreement, Cost, Password, Role_idRole) VALUES (:company,:address,:email,:phone,:name,:firstname,:agreement,:cost,:password, 2)' );
            foreach($array as $key => &$value) {
                $proprepare->bindParam(":" . $key, $value);
            }
            $proprepare->execute();
        }catch(PDOException $e){
            echo $e->getMessage();
        }

    }

    public static function login($array){
        $userpassword = $array['password'];
        $useremail = $array['email'];

        try{
            $db = new PDO('mysql:host=localhost;dbname=garderie', 'damien', 'root');
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $userhash = $db->query("SELECT Password FROM user WHERE Email = '$useremail'")->fetch();
            if($userhash == false){
                $userhash = $db->query("SELECT Password FROM professional WHERE Email = '$useremail'")->fetch();
                if(password_verify($userpassword, $userhash[0])) {
                    $userlogin = $db->query("SELECT * FROM USER WHERE Email = '$useremail' AND Password = '$userhash[0]'")->fetchAll(PDO::FETCH_ASSOC);
                    if($userlogin == false ){
                        $userlogin = $db->query("SELECT * FROM professional WHERE Email = ")->fetchAll(PDO::FETCH_ASSOC);
                        // var_dump($userlogin);
                        session_start();
                        $_SESSION['user'] = $userlogin[0];
                    }
                }
            }elseif(password_verify($userpassword,$userhash[0])){
                if(password_verify($userpassword, $userhash[0]))
                    $userlogin = $db->query("SELECT * FROM professional WHERE Email = '$useremail' AND Password = '$userhash[0]'")->fetchAll(PDO::FETCH_ASSOC);
                    // var_dump($userlogin);
                    session_start();
                    $_SESSION['user'] = $userlogin[0];
                    var_dump($_SESSION['user']);
            } else{
                echo 'erreur';
            }
        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}