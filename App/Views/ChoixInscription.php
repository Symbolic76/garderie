<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 09/11/2018
 * Time: 11:54
 */
?>


<div class="container">

    <h1>Choisissez votre type d'utilisateur</h1>

    <div class="row">

        <div class="col">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="<?php PUB_PATH ?>/Public/Assets/Images/test.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Inscription Professionnelle</h5>
                    <p class="card-text">Inscrivez vous en tant que professionnel de la garde d'enfants.</p>
                    <a href="InscriptionPro.php" class="btn btn-primary">Inscription</a>
                </div>
            </div>
        </div>

        <div class="col">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="<?php PUB_PATH ?>/Public/Assets/Images/test.jpg" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">Inscription Parents</h5>
                    <p class="card-text">Inscrivez vous en tant que parent pour enregistrer vos enfants sur le site.</p>
                    <a href="Inscription.php" class="btn btn-primary">Inscription</a>
                </div>
            </div>
        </div>

    </div>
</div>
