<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 15/11/2018
 * Time: 19:24
 */
?>

<div class="main-content">
    <div class="connection-form">
        <form class="register col" action="" method="post">

            <h3 class="title">Connectez vous !</h3>

            <div class="input-email">
                <input id="connect-email" type="email" class="email" name="data[email]" value="" placeholder="Email">
            </div>

            <div class="input-password">
                <input id="icon_prefix" type="password" class="validate" name="data[password]" value="" placeholder="Mot de passe">
            </div>

            <div class="center-align">
                <button class="btn-primary" type="submit" name="action">Connexion</button>
            </div>

        </form>
    </div>


</div>