<?php

?>

<form name="formInscription" method="post" >
    <div class="container">
        <h1>Inscription Utilisateur</h1>
        <p>Remplissez ce formulaire pour créer un compte</p>
        <hr>

        <div class="row">
            <div class="col">
                <label for="lastname"><b>Nom</b></label>
                <input class="form-control" type="text" placeholder="Nom" name="data[name]" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="firstname"><b>Prénom</b></label>
                <input class="form-control" type="text" placeholder="Prénom" name="data['firstname']" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="address"><b>Adresse</b></label>
                <input class="form-control" type="text" placeholder="Adresse" name="data['address']" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="téléphone"><b>Téléphone</b></label>
                <input class="form-control" type="tel" placeholder="Téléphone" name="data['phone']" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="email"><b>Adresse Email</b></label>
                <input class="form-control" type="email" placeholder="Email" name="data['email']" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="password"><b>Mot de passe</b></label>
                <input class="form-control" type="password" placeholder="Mot de passe" name="data['password']" required>
            </div>
        </div>

        <!--<div class="row">
                <div class="col">
                    <label for="captcha"><b>Complétez le captcha</b></label>
                    <img src="captcha.php" />
                    <input class="form-control" type="text" name="captcha" />
                </div>
            </div>-->
        <hr>
        <button type="submit" name="action" class="btn btn-dark">Inscription</button>
    </div>
</form>
    <div class="container signin">
        <p>Vous avez déjà un compte ? <a href="#">Connectez Vous</a>.</p>
    </div>
