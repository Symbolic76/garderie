<?php

?>

<form method="post" action="">
    <div class="container">
        <h1>Inscription Professionnelle</h1>
        <p>Remplissez ce formulaire pour créer un compte en tant que professionnel</p>
        <hr>

        <div class="row">
            <div class="col">
                <label for="society"><b>Societe</b></label>
                <input class="form-control" type="text" placeholder="Societe" name="society" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="lastname"><b>Nom</b></label>
                <input class="form-control" type="text" placeholder="Nom" name="Nom" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="firstname "><b>Prénom</b></label>
                <input class="form-control" type="text" placeholder="Prénom" name="Prenom" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="Email"><b>Adresse Email</b></label>
                <input class="form-control" type="email" placeholder="Adresse Email" name="Email" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="repeatemail"><b>Validation Adresse Email</b></label>
                <input class="form-control" type="email" placeholder="Adresse Email" name="repeatemail" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="Pass"><b>Mot de passe</b></label>
                <input class="form-control" type="password" placeholder="Mot de passe" name="Pass" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="repeatpassword"><b>Validation mot de passe</b></label>
                <input class="form-control" type="password" placeholder="Mot de passe" name="repeatpassword" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="Adresse"><b>Adresse</b></label>
                <input class="form-control" type="text" placeholder="Adresse" name="Adresse" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="Telephone"><b>N° de téléphone</b></label>
                <input class="form-control" type="text" placeholder="N° de téléphone" name="Telephone" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="agreement"><b>N° d'agrément</b></label>
                <input class="form-control" type="text" placeholder="N° d'agrément" name="agreement" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="cost"><b>Cout horaire</b></label>
                <input class="form-control" type="text" placeholder="Cout horaire" name="cost" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="creche"><b>Nom de l'établissement</b></label>
                <input class="form-control" type="text" placeholder="Nom de l'établissement" name="creche" required>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <label for="photo"><b>Photo de profil</b></label>
                <input class="form-control" type="file" name="photo">
            </div>
        </div>
        <hr>

        <button type="submit" name="submit" class="btn btn-dark">Register</button>
        <p>En créant un compte vous acceptez les <a href="#">Terms & Privacy</a>.</p>

        <button type="submit" class="btn btn-dark">S'inscire</button>

    </div>

    <div class="container signin">
        <p>Vous avez déjà un compte ? <a href="#">Connectez vous</a>.</p>
    </div>
</form>


