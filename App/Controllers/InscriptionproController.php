<?php


     class InscriptionproController
     {
         public function inscriptionPro()
         {
             include '../Views/InscriptionPro.php.php';

             if(isset($_POST['inscription']))
             {
                 $_POST['Nom'] = htmlspecialchars($_POST['Nom']);
                 $_POST['Prenom'] = htmlspecialchars($_POST['Prénom']);
                 $_POST['Email'] = htmlspecialchars($_POST['Email']);
                 $_POST['Pass'] = htmlspecialchars($_POST['Pass']);
                 $_POST['repeatpassword'] = htmlspecialchars($_POST['repeatpassword']);
                 $_POST['Cost'] = htmlspecialchars($_POST['Cost']);
                 $_POST['agreement'] = htmlspecialchars($_POST['agreement']);
                 $_POST['Telephone'] = htmlspecialchars($_POST['Telephone']);
                 $_POST['Adresse'] = htmlspecialchars($_POST['Adresse']);
                 $_POST['creche'] = htmlspecialchars($_POST['creche']);

                 if(preg_match("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $_POST['Email']))
                 {
                     $Email_valide = true;
                 }


                 if(!isset($_POST['Pass']) OR !isset($_POST['repeatpassword']) OR empty($_POST['Pass']) OR $_POST['Pass'] != $_POST['repeatpassword'])
                 {
                     echo 'Les deux mots de passes ne sont pas identiques, veuillez les saisir à nouveau';
                 }

                 elseif(empty($_POST['Email']) OR !isset($Email_valide))
                 {
                     echo 'L\'adresse e-mail n\'est pas valide !';
                 }

                 else
                 {

                    echo password_hash($_POST['Pass'] , PASSWORD_DEFAULT)."\n";

                }
             }
         }
     }