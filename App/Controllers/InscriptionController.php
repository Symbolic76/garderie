<?php

/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 16/11/2018
 * Time: 10:44
 */

class InscriptionController
{
    // Fonction qui valide les formulaires inscription (parent) et inscription (pro)
    public function index(){
        include VIEW_PATH.'Inscription.php';
        include MDL_PATH.'User/User.php';

        if (isset($_POST['action'])){
            $data = $_POST['data'];
            foreach ($data as $key => $value){
                if(empty($value)){
                    echo 'Tous les champs ne sont pas valides';
                }
            }
            User::register($data);
        }

        if(isset($_POST['proaction'])){
            $data = $_POST['data'];
            foreach($data as $key => $value){
                if(empty($value)){
                    echo 'Tous les champs ne sont pas valides';
                }
            }
            User::registerPro($data);
        }
    }
}
