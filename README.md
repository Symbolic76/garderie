# Garderie

Language utilisé :

- HTML // CSS
- PHP
- Java Script
- Sur une architecture MVC

                                    
                                    
                                    
Utilité du site :

"Myderie" (nom provisoire) est un site permettant à des particuliers d'entrer en contact avec des professionnelles de garde d'enfant.
Mettant ainsi en valeurs les crèches et nourrices de Rouen.


Fonctionnalité : 

- Inscription // Connexion, en tant que particulier ou professionnelle.
- Désinscription.

En tant que Parents (Particulier):

- Afficher ses information ( Prénom, Nom, Age).
- Possibilité d'ajouter ses enfants. ( Tout en pouvant préciser certains type d'handicape )
- Rechercher les Garderie, et les nourrices, de Rouen disponible.
- Entrer en contact avec un professionnelle via un Chat.
- Précision sur de tiers personne pouvant récupérer les enfants.
- Supprimer un enfant de son compte.
- Deconnexion.

En tant que Crèche ou Nourrice (Professionnelle) :

- Afficher ses informations ( Prénom, Nom, Age).
- Afficher ses qualifications et ses experiences professionnelle.
- Définir un tarif en euros.
- Définir ses disponibilités.
- Répondre au particulier via un chat.
- Deconnexion