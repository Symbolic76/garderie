<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 12/11/2018
 * Time: 11:55
 */

class Model
{

    private static $connect = NULL;
    private $bdd;


    public function __construct()
    {
        $strBddServer = "localhost";
        $strBddLogin = "damien";
        $strBddPassword = "root";
        $strBddBase = "garderie";

        /** Lien à la BDD */
        try{
            $this->bdd = new PDO('mysql:host=' .$strBddServer. ';dbname=' .$strBddBase,$strBddLogin,$strBddPassword, array(PDO::MYSQL_ATTR_INIT_COMMAND =>'SET NAMES utf8'));
            $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }

    public function getInstance() {
        if(is_null(self::$connect)) {
            self::$connect = new Model();
        }
        return self::$connect;
    }

    /** FONCTIONS */

    /** Effectue une requete SQL. Retourne le résultat s'il y en a un, sous forme d'objet */
    public function requete($req){
        $query = $this->bdd->query($req);
        return $query;
    }

    /** Permet de préparer une requete SQL. Retourne la requete préparée sous forme d'objet */
    public function préparation($req){
        $query = $this->bdd->prepare($req);
        return $query;
    }

    /** Permet d'éxécuter une requete SQL préparée. Retourne le résultat s'il y en a un, sous forme d'objet.*/
    public function execution($query, $tab){
        $req = $query->execute($tab);
        return $req;
    }

    /** Retourne l'id de la dernière insertion par auto-incrément dans la BDD */
    public function dernierIndex(){
        return $this->bdd->lastInsertId();
    }

}