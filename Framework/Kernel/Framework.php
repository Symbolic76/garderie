<?php
/**
 * Created by PhpStorm.
 * User: Administrateur
 * Date: 12/11/2018
 * Time: 14:24
 */

require_once './App/Controllers/UserController.php';
require_once './App/Controllers/InscriptionController.php';

class Framework
{
    public static function run(){
        self::initialize();
        self::autoloader();
        self::header();
        self::switcher();
        self::footer();
    }

    private static function initialize(){
        $getParamUrl = $_SERVER['REQUEST_URI'];
        $getParamUrlArray = explode("/", $getParamUrl);

        define('DIRSEP', DIRECTORY_SEPARATOR);
        define('ROOT', getcwd() .DIRSEP);
        define('APP_PATH', ROOT.'App'.DIRSEP);
        define('PUB_PATH', dirname($_SERVER['SCRIPT_NAME']));
        define('FRAMEWORK_PATH', ROOT.'Framework'.DIRSEP);
        define('ASS_PATH', PUB_PATH.'Assets'.DIRSEP);
        define('CONF_PATH', APP_PATH. 'Conf' .DIRSEP);
        define('CTRL_PATH', APP_PATH. 'Controllers'.DIRSEP);
        define('MDL_PATH', APP_PATH. 'Models' .DIRSEP);
        define('VIEW_PATH', APP_PATH. 'Views' .DIRSEP );
        define('FORM_PATH', VIEW_PATH. 'Forms' .DIRSEP);
        define('LAYOUT_PATH', VIEW_PATH. 'Layout' .DIRSEP);
        define('UNKNOWN_PAGE', LAYOUT_PATH.'404.php');
        define('KERNEL_PATH', FRAMEWORK_PATH. 'Kernel' .DIRSEP);
        define('LIB_PATH', FRAMEWORK_PATH. 'Libraries' .DIRSEP);
        if (count($getParamUrlArray) == 4) {
            if ($getParamUrlArray[2] != "" && $getParamUrlArray[3] != "") {
                define('CONTROLLER', $getParamUrlArray[2]);
                define('ACTION', $getParamUrlArray[3]);
            }
        }
    }

    private static function autoloader(){
        spl_autoload_register(array(__CLASS__, 'loading'));
    }

    private static function loading($class){
        if(substr($class, -10) == "Controller"){
            require_once "Framework.php";

        }elseif(substr($class, -5) == "Model"){
            require_once "Framework.php";
        }
    }

    private static function switcher(){
        $getParamUrl = $_SERVER['REQUEST_URI'];;
        $getParamUrlArray = explode("/", $getParamUrl);

        if (count($getParamUrlArray) == 3 && $getParamUrlArray[2] == "") {
              include LAYOUT_PATH . "Accueil.php";
        }else{
            if (isset($getParamUrlArray[3])) {
                if ($getParamUrlArray[2] != "" && $getParamUrlArray[3] != "") {
                    if (file_exists(CTRL_PATH . CONTROLLER . "Controller.php")) {
                        $controllerName = CONTROLLER . "Controller";
                        $actionName = ACTION;

                        $controller = new $controllerName;
                        if (method_exists($controller, ACTION)) {
                            $controller->$actionName();
                        } else {
                            include UNKNOWN_PAGE;
                        }
                    } else {
                        include UNKNOWN_PAGE;
                    }
                } else {
                    include UNKNOWN_PAGE;
                }
            } else {
                include UNKNOWN_PAGE;
            }
            }
    }

    private static function header(){
        include_once LAYOUT_PATH.'header.php';
    }

    private static function footer(){
        include_once LAYOUT_PATH.'footer.php';
    }
}